# Survey Rapid Test Covid-19

Survey Rapid Test Covid-19 adalah sebuah program berupa formulir survey pendapat tentang diadakannya Rapid Test Covid-19. Program ini dibuat menggunakan Java EJB, Servlet, dan JSP yang terkoneksi ke database.

## Installation


- Nyalakan MySQL dan Glassfish Server.

- Deploy menggunakan browser dengan url : http://localhost:8080/SurveyRapidTest/

## ScreenShoot

![Annotation_2020-06-19_141629](/uploads/2e36c1f892252e51325ede052748775a/Annotation_2020-06-19_141629.png)

![Annotation_2020-06-19_142042](/uploads/bc4e690998a6b1edb2e50ffeaceef0d2/Annotation_2020-06-19_142042.png)

![Annotation_2020-06-19_142917](/uploads/a1cfcc8d37a73fb225bdbc52ef23e222/Annotation_2020-06-19_142917.png)

![Annotation_2020-06-19_141850](/uploads/558b6345a59784503266494a6d09d948/Annotation_2020-06-19_141850.png)
